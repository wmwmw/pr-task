from colorhash import ColorHash
import xmltodict

import aiosqlite
import aiohttp

from sanic import Sanic
from sanic import json


app = Sanic(__name__)


BASE_RESULTS_URL = "https://volby.cz/pls/ps2017nss/vysledky_okres"


def dict_factory(cursor, row):
    """ Used for Sqlite client to return results in dicts, ref: https://docs.python.org/3/library/sqlite3.html#sqlite3.Connection.row_factory """
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return d


async def get_results_for_city(city):
    """ Fetch results for city from volby.cz server """
    async with app.ctx.http_client.get(BASE_RESULTS_URL, params={"nuts": city["district_id"]}) as response:
        data = xmltodict.parse(await response.text())

        try:
            results = next(item["HLASY_STRANA"] for item in data["VYSLEDKY_OKRES"]
                           ["OBEC"] if item["@CIS_OBEC"] == city["city_id"])
        except StopIteration:
            return None

        return [{"party_id": result["@KSTRANA"], "votes": int(result["@HLASY"]), "p_votes": float(result["@PROC_HLASU"])} for result in results]


async def get_parties_map():
    """ {party_id: short_name} mapping for extending results returned from the volby.cz server """
    cur = await app.ctx.db.execute("select * from party")
    parties = await cur.fetchall()
    return {party["party_id"]: party["short_name"] for party in parties}


app.static('/', './chart.html', name='index')


@app.listener("before_server_start")
async def init(app, loop):
    # database
    app.ctx.db = await aiosqlite.connect("./ps2017.db", loop=loop)
    app.ctx.db.row_factory = dict_factory

    # aioclient
    app.ctx.http_client = aiohttp.ClientSession(loop=loop)


@app.listener("after_server_stop")
async def teardown(app, _loop):
    await app.ctx.db.close()
    await app.ctx.http_client.close()


@app.route("/api/city/", methods=["GET"])
async def cities_list(_req):
    cur = await app.ctx.db.execute("select * from city order by name")
    cities = await cur.fetchall()
    return json(cities)


@app.route("/api/results/<city_id>/", methods=["GET"])
async def get_city(_req, city_id):
    cur = await app.ctx.db.execute("select * from city where city_id=?", (city_id, ))
    city = await cur.fetchone()

    if not city:
        return json({"message": "Not found"}, status=404)

    parties_map = await get_parties_map()
    results = await get_results_for_city(city)

    # extend results, from volby.cz it doesnt include short_name of parties
    results = [result | {"short_name": parties_map[result["party_id"]]}
               for result in results]

    # might be a bit more efficient, but for readability
    labels = [x["short_name"] for x in results]
    values = [x["votes"] for x in results]

    # data formatted for Chart.js
    response = {
        "labels": labels,
        "datasets": [
            {
                "data": values,
                # calculate hex hash from short_name, so its same every time
                "backgroundColor": [ColorHash(x["short_name"]).hex for x in results]
            }
        ]
    }

    return json(response)

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8000)

# What could be improved
# - caching, when the source server is slow or the app is under heavy load, current approach is rather naive
# - error handling, due to questionable open data formats it might be unpredictable, some keys might not exist (FE+BE)
# - efficiency, it would be better to use more efficient approaches to parse and use XML data, but current is readable and convenient
# - FE is naive and select is slow for the not so small number of cities
